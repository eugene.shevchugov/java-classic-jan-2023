package ru.classic.lesha;

import ru.classic.string.StringSimplifier;

public class LeshaStringSimplifier implements StringSimplifier {

  @Override
  public String summaryForFrontView(Integer summary) {
    return String.format("Summary is %s rub.", summary);
  }

  @Override
  public String formatDate(String date) {
    return date.replaceAll("\\D+", " ").trim().replaceAll("\\s+", ".");
  }

  @Override
  public String cleanTrash(String text) {
    return text.replaceAll("[^a-zA-Z\\s]", "").replaceAll("\\s+", " ").trim();
  }

  @Override
  public String print(Integer... array) {
    var result = new StringBuilder();
    var sum = 0;

    for (int element : array) {
      result.append(element).append("\t").append("sum = ").append(sum += element).append("\n");
    }
    return result.toString();
  }

  /* Для оптимизации можно добавить в начало проверку на нулевой массив
   * или после while добавить проверку на нулевой префикc -> return "",
   * но для лаконичности оставил так */
  @Override
  public String getOverallBeginning(String[] array) {
    String prefix = array[0];
    for (int i = 1; i < array.length; i++) {
      while (array[i].indexOf(prefix) != 0) {
        prefix = prefix.substring(0, prefix.length() - 1);
      }
    }
    return prefix;
  }

  public static void main(String[] args) {
    var simplifier = new LeshaStringSimplifier();
    System.out.print(simplifier.summaryForFrontView(5_000_000) + "\n" +
        simplifier.formatDate("It's day: 25, It's month: 10, It's year: 2019. Format it!") + "\n" +
        simplifier.cleanTrash(
            "  Reasons to go to university is пары в пятницу с 14:30 до 17:50 а еще их ведет Усов и домой по самым пробкам")
        + "\n" +
        simplifier.print(1, 2, 43554363, 23, 214, 454364634, 75, 4) +
        simplifier.getOverallBeginning(new String[]{"poor", "polite", "police"}) + "\n" +
        simplifier.getOverallBeginning(new String[]{"asd", "asd", "pokv"})
    );
  }
}
