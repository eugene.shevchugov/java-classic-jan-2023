package ru.classic.juli.factory;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListFactory<E> {
    public List<E> createList(ListType type) {
        if (type == null) {
            throw new IllegalArgumentException("Task type should be not null.");
        }
        switch (type) {
            case ARRAY:
                return new ArrayList<>();
            case LINKED:
                return new LinkedList<>();
            default:
                throw new IllegalArgumentException("Unsupported task type: " + type);
        }
    }
}



