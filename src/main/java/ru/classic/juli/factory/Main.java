package ru.classic.juli.factory;


import java.util.List;

public class Main {
    public static void main(String[] args) {
        ListFactory<Integer> integerListFactory = new ListFactory<>();
        List<Integer> list = integerListFactory.createList(ListType.ARRAY);
        System.out.println(list.getClass());
    }
}