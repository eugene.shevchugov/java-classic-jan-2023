package ru.classic.juli.stringSimplifier;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException {
        JuliStringSimplifier juliStringSimplifier = new JuliStringSimplifier();
        System.out.println(juliStringSimplifier.summaryForFrontView(1038835));
        System.out.println(juliStringSimplifier.formatDate("dg dfg 01 egergg 02 regerg 2000 ddfhfhf"));
        System.out.println(juliStringSimplifier.cleanTrash("5  d  sdd94t   sdkf434  "));
        System.out.println(juliStringSimplifier.print(2, 1, 4));
        String[] array = {"poor", "polite", "police"};
        System.out.println(juliStringSimplifier.getOverallBeginning(array));
    }
}