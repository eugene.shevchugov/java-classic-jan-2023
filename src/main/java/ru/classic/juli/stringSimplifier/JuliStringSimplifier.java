package ru.classic.juli.stringSimplifier;

import ru.classic.string.StringSimplifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JuliStringSimplifier implements StringSimplifier {
    @Override
    public String summaryForFrontView(Integer summary) {
        return String.format("Summary is %d rub.", summary);
    }

    @Override
    public String formatDate(String date) {
        String regex = "(\\d{2,4})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(date);
        String rez = "";
        while (matcher.find()) {
            rez += matcher.group(1) + ".";
        }
        return rez.substring(0, rez.length() - 1);
    }

    @Override
    public String cleanTrash(String text) {
        return text.replaceAll("[^a-zA-Zа ]", "").trim().replaceAll("\\s+", " ");
    }

    @Override
    public String print(Integer... array) {
        Integer summ = 0;
        String rezStr = "";
        for (Integer integer : array) {
            summ += integer;
            rezStr += integer + " elem " + summ + "\n";
        }
        return rezStr;
    }

    @Override
    public String getOverallBeginning(String[] array) {
        String str = array[0];
        for (int i = 1; i < array.length; i++) {
            while (array[i].indexOf(str) != 0) {
                str = str.substring(0, str.length() - 1);
                if (str.isEmpty()) {
                    return "";
                }
            }
        }
        return str;
    }

}
