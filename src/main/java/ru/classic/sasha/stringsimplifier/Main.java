package ru.classic.sasha.stringsimplifier;

public class Main {
    public static void main(String[] args) {
        SashaStringSimplifier sashaStringSimplifier = new SashaStringSimplifier();
        System.out.println(sashaStringSimplifier.summaryForFrontView(1000));
        System.out.println(sashaStringSimplifier.formatDate("safsaf12saaf12dfsag1222dgsg"));
        System.out.println(sashaStringSimplifier.cleanTrash(". 12 dsag 23    saf aef     fadfa"));
        System.out.println(sashaStringSimplifier.print(1,2,3,4,5));
    }
}
