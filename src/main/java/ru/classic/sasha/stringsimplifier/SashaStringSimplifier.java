package ru.classic.sasha.stringsimplifier;

import ru.classic.string.StringSimplifier;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SashaStringSimplifier implements StringSimplifier {

    @Override
    public String summaryForFrontView(Integer summary) {
        return String.format("Summary is %s rub.", summary);
    }

    @Override
    public String formatDate(String date) {
        return Arrays.stream(date.replaceAll("\\D+", "-").split("-"))
                .filter(s -> !s.isEmpty())
                .collect(Collectors.joining("."));
    }

    @Override
    public String cleanTrash(String text) {
        return text.replaceAll("[\\d]", "").replaceAll("\s+", " ").trim();
    }

    @Override
    public String print(Integer... array) {
        StringBuilder resulStr = new StringBuilder();
        Integer sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            resulStr.append(String.format("\n%s\t sum =  %s", i + 1, sum));
        }

        return resulStr.toString();
    }

    @Override
    public String getOverallBeginning(String[] array) {
        return null;
    }

}
