package ru.classic.sasha.hashSet;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class MyHashSet<E> extends AbstractSet<E> implements Set<E> {
    private HashMap<E, Object> map = new HashMap();
    private final Object object = new Object();

    @Override
    public boolean add(E element) {
        return map.put(element, object) == null;
    }

    @Override
    public boolean remove(Object element) {
        return map.remove(element, object);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean contains(Object element) {
        return map.containsKey(element);
    }

    public int size() {
        return map.size();
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

}
