package ru.classic.dima.task2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StatisticsManager {

  private final Map<String, Collection<Long>> executionDataMap = new HashMap<>();

  public void submit(String methodName, long nanos) {
    executionDataMap.computeIfAbsent(methodName, k -> new ArrayList<>()).add(nanos);
  }

  public String getAverageTime(String methodName) {
    Collection<Long> data = executionDataMap.get(methodName);

    long sum = 0;
    for (long time : data) {
      sum += time;
    }

    long averageTime = sum / data.size();

    return String.format(
        "%s average time is %d ,num of calls is %d", methodName, averageTime, data.size());
  }
}
