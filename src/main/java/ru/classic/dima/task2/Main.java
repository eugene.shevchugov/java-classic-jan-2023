package ru.classic.dima.task2;

import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    StatisticsManager statisticsManager = new StatisticsManager();
    List<String> names = new StatisticsProxyList<>(new ArrayList<>(), statisticsManager);

    List<String> arrayText = List.of("Bob", "Alice", "Asata", "Teo", "Abema");

    for (int i = 0; i <= 10000; i++) {
      names.add(String.valueOf(i));
      names.add(0, String.valueOf(i));
      names.remove(String.valueOf(i));
      names.containsAll(arrayText);
      names.addAll(arrayText);
      names.addAll(0,  arrayText);
      names.get(0);
      names.set(0, String.valueOf(i));
      names.removeAll(arrayText);
      names.retainAll(arrayText);
      names.isEmpty();
      names.indexOf(String.valueOf(i));
      names.lastIndexOf(String.valueOf(i));
    }

    System.out.println(statisticsManager.getAverageTime("add"));
    System.out.println(statisticsManager.getAverageTime("addByIndex"));
    System.out.println(statisticsManager.getAverageTime("remove"));
    System.out.println(statisticsManager.getAverageTime("containsAll"));
    System.out.println(statisticsManager.getAverageTime("addAll"));
    System.out.println(statisticsManager.getAverageTime("addAllByIndex"));
    System.out.println(statisticsManager.getAverageTime("removeAll"));
    System.out.println(statisticsManager.getAverageTime("retainAll"));
    System.out.println(statisticsManager.getAverageTime("isEmpty"));
    System.out.println(statisticsManager.getAverageTime("get"));
    System.out.println(statisticsManager.getAverageTime("set"));
    System.out.println(statisticsManager.getAverageTime("indexOf"));
    System.out.println(statisticsManager.getAverageTime("lastIndexOf"));
  }
}
