package ru.classic.dima.task2;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class StatisticsProxyList<E> implements List<E> {

  private final List<E> implList;
  private final StatisticsManager statisticsManager;

  public StatisticsProxyList(List<E> implList, StatisticsManager statisticsManager) {
    this.implList = implList;
    this.statisticsManager = statisticsManager;
  }

  @Override
  public int size() {
    return implList.size();
  }

  @Override
  public boolean isEmpty() {
    long startTime = System.nanoTime();
    boolean result = implList.isEmpty();
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("isEmpty", executionTime);
    return result;
  }

  @Override
  public boolean contains(Object o) {
    long startTime = System.nanoTime();
    boolean result = implList.contains(o);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("contains", executionTime);
    return result;
  }

  @Override
  public Iterator<E> iterator() {
    return implList.iterator();
  }

  @Override
  public Object[] toArray() {
    return implList.toArray();
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return implList.toArray(a);
  }

  @Override
  public boolean add(E e) {
    long startTime = System.nanoTime();
    boolean result = implList.add(e);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("add", executionTime);
    return result;
  }

  @Override
  public boolean remove(Object o) {
    long startTime = System.nanoTime();
    boolean result = implList.remove(o);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("remove", executionTime);
    return result;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    long startTime = System.nanoTime();
    boolean result = implList.containsAll(c);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("containsAll", executionTime);
    return result;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    long startTime = System.nanoTime();
    boolean result = implList.addAll(c);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("addAll", executionTime);
    return result;
  }

  @Override
  public boolean addAll(int index, Collection<? extends E> c) {
    long startTime = System.nanoTime();
    boolean result = implList.addAll(index, c);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("addAllByIndex", executionTime);
    return result;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    long startTime = System.nanoTime();
    boolean result = implList.removeAll(c);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("removeAll", executionTime);
    return result;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    long startTime = System.nanoTime();
    boolean result = implList.retainAll(c);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("retainAll", executionTime);
    return result;
  }

  @Override
  public void clear() {
    long startTime = System.nanoTime();
    implList.clear();
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("clear", executionTime);
  }

  @Override
  public E get(int index) {
    long startTime = System.nanoTime();
    E result = implList.get(index);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("get", executionTime);
    return result;
  }

  @Override
  public E set(int index, E element) {
    long startTime = System.nanoTime();
    E result = implList.set(index, element);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("set", executionTime);
    return result;
  }

  @Override
  public void add(int index, E element) {
    long startTime = System.nanoTime();
    implList.add(index, element);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("addByIndex", executionTime);
  }

  @Override
  public E remove(int index) {
    long startTime = System.nanoTime();
    E result = implList.remove(index);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("addByIndex", executionTime);
    return result;
  }

  @Override
  public int indexOf(Object o) {
    long startTime = System.nanoTime();
    int result = implList.indexOf(o);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("indexOf", executionTime);
    return result;
  }

  @Override
  public int lastIndexOf(Object o) {
    long startTime = System.nanoTime();
    int result = implList.lastIndexOf(o);
    long executionTime = System.nanoTime() - startTime;
    statisticsManager.submit("lastIndexOf", executionTime);
    return result;
  }

  @Override
  public ListIterator<E> listIterator() {
    return implList.listIterator();
  }

  @Override
  public ListIterator<E> listIterator(int index) {
    return implList.listIterator(index);
  }

  @Override
  public List<E> subList(int fromIndex, int toIndex) {
    return implList.subList(fromIndex, toIndex);
  }
}
