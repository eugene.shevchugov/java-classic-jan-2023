package ru.classic.dima.task5;

import ru.classic.string.StringSimplifier;

public class DimaStringSimplifier implements StringSimplifier {

  @Override
  public String summaryForFrontView(Integer summary) {
    return String.format("Summary is %d rub.", summary);
  }

  @Override
  public String formatDate(String date) {
    return date.replaceAll("\\D+", " ").trim().replaceAll("\\s+", ".");
  }

  @Override
  public String cleanTrash(String text) {
    return text.replaceAll("[^a-zA-Z\\s]", "").replaceAll("\\s+", " ").trim();
  }

  @Override
  public String print(Integer... array) {

    StringBuilder textResult = new StringBuilder();
    Integer sum = 0;

    for (Integer number : array) {
      sum += number;
      textResult.append(number).append("\tsum = ").append(sum).append("\n");
    }

    return textResult.toString();
  }

  @Override
  public String getOverallBeginning(String[] array) {
    int indexSymbolInWord = 0;
    char symbol = array[0].charAt(indexSymbolInWord);
    String beginningWord = "";
    boolean result = true;

    while (result) {
      for (String word : array) {
        if (word.charAt(indexSymbolInWord) != symbol) {
          return beginningWord;
        }
      }
      beginningWord += symbol;
      indexSymbolInWord++;
      symbol = array[0].charAt(indexSymbolInWord);
    }
    return beginningWord;
  }
}
