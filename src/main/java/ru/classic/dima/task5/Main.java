package ru.classic.dima.task5;

public class Main {

  public static void main(String[] args) {

    DimaStringSimplifier dima = new DimaStringSimplifier();

    System.out.println(dima.summaryForFrontView(12));
    System.out.println(dima.formatDate("*[\\D+]11*[\\D+]10*[\\D+]1999"));
    System.out.println(dima.cleanTrash("Абема and onton решил chto 123 on крутой .--%"));
    System.out.println(dima.print(1, 2, 3, 4, 5, 6, 7));
    System.out.println(dima.getOverallBeginning(new String[]{"Abebema", "Abebama", "Abeboma", "Abebima"}));
  }
}
