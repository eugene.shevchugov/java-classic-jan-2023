package ru.classic.dima.task4;

import java.util.Objects;

public class Woman implements Human {

  private String name;

  public Woman(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Woman woman = (Woman) o;
    return Objects.equals(name, woman.name);
  }

  @Override
  public int hashCode() {
    return 999;
  }
}
