package ru.classic.dima.task4;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class mapMain {

  public static void main(String[] args) {

    HashMap<Human, Integer> testMap = new HashMap<>();
    Woman woman = new Woman("Jessica");
    Man man = new Man("Abema");

    for (int i = 1; i < 10001; i++) {
      testMap.put(new Woman(generateRandomName(15)), i);
    }

    System.out.println("Скорость выполнения методов в HashMap с ключом Woman: \n"
        + "cкорость создания нового элемента: " + getCreateElementTime(testMap, woman, 99)
        + "\ncкорость чтения элемента: " + getReadElementTime(testMap, woman)
        + "\ncкорость обновления элемента: " + getUpdateElementTime(testMap, woman, 110)
        + "\ncкорость удаления элемента: " + getDeleteElementTime(testMap, woman));

    testMap.clear();
    for (int i = 1; i < 10001; i++) {
      testMap.put(new Man(generateRandomName(15)), i);
    }

    System.out.println("\n\nСкорость выполнения методов в HashMap с ключом Man: \n"
        + "cкорость создания нового элемента: " + getCreateElementTime(testMap, man, 99)
        + "\ncкорость чтения элемента: " + getReadElementTime(testMap, man)
        + "\ncкорость обновления элемента: " + getUpdateElementTime(testMap, man, 110)
        + "\ncкорость удаления элемента: " + getDeleteElementTime(testMap, man));
  }

  private static String generateRandomName(int lengthName) {

    Random random = new Random();
    String name = "";

    for (int i = 0; i < lengthName; i++) {
      int number = random.nextInt(65, 91);
      name += String.valueOf((char) number);
    }
    return name;
  }

  private static long getCreateElementTime(Map<Human, Integer> testMap, Human human,
      Integer number) {

    long startTime = System.nanoTime();

    testMap.putIfAbsent(human, number);

    return System.nanoTime() - startTime;
  }

  private static long getReadElementTime(Map<Human, Integer> testMap, Human human) {

    long startTime = System.nanoTime();

    testMap.get(human);

    return System.nanoTime() - startTime;
  }

  private static long getUpdateElementTime(Map<Human, Integer> testMap, Human human,
      Integer number) {

    long startTime = System.nanoTime();

    testMap.put(human, number);

    return System.nanoTime() - startTime;
  }

  private static long getDeleteElementTime(Map<Human, Integer> testMap, Human human) {

    long startTime = System.nanoTime();

    testMap.remove(human);

    return System.nanoTime() - startTime;
  }
}
