package ru.classic.dima.task3;

import java.util.List;

public class ListTest {

  List<Integer> list;

  public ListTest(List<Integer> list) {
    this.list = list;
  }

  // Добавление
  public long getAddFirstElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.add(0, number);

    return System.nanoTime() - startTime;
  }

  public long getAddMiddleElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.add(list.size() / 2, number);

    return System.nanoTime() - startTime;
  }

  public long getAddLastElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.add(list.size() - 1, number);

    return System.nanoTime() - startTime;
  }


  // Изменение
  public long getSetFirstElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.set(0, number);

    return System.nanoTime() - startTime;
  }

  public long getSetMiddleElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.set(list.size() / 2, number);

    return System.nanoTime() - startTime;
  }

  public long getSetLastElementTime(Integer number) {

    long startTime = System.nanoTime();

    list.set(list.size() - 1, number);

    return System.nanoTime() - startTime;
  }

  // Поиск
  public long getGetFirstElementTime() {

    long startTime = System.nanoTime();

    list.get(0);

    return System.nanoTime() - startTime;
  }

  public long getGetMiddleElementTime() {

    long startTime = System.nanoTime();

    list.get(list.size() / 2);

    return System.nanoTime() - startTime;
  }

  public long getGetLastElementTime() {

    long startTime = System.nanoTime();

    list.get(list.size() - 1);

    return System.nanoTime() - startTime;
  }

  // Удаление
  public long getRemoveFirstElementTime() {

    long startTime = System.nanoTime();

    list.remove(0);

    return System.nanoTime() - startTime;
  }

  public long getRemoveMiddleElementTime() {

    long startTime = System.nanoTime();

    list.remove(list.size() / 2);

    return System.nanoTime() - startTime;
  }

  public long getRemoveLastElementTime() {

    long startTime = System.nanoTime();

    list.remove(list.size() - 1);

    return System.nanoTime() - startTime;
  }
}
