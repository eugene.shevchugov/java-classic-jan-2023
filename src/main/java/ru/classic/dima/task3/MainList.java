package ru.classic.dima.task3;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainList {

  public static void main(String[] args) {

    ArrayList<Integer> arrayList = new ArrayList<>();
    LinkedList<Integer> linkedList = new LinkedList<>();

    int number = 10;
    long startTimeArrayList, executionTimeArrayList;
    long startTimeLinkedList, executionTimeLinkedList;

    startTimeArrayList = System.nanoTime();
    for (int i = 0; i < 50000; i++) {
      arrayList.add(i, i);
    }
    executionTimeArrayList = System.nanoTime() - startTimeArrayList;

    startTimeLinkedList = System.nanoTime();
    for (int i = 0; i < 50000; i++) {
      linkedList.add(i, i);
    }
    executionTimeLinkedList = System.nanoTime() - startTimeLinkedList;

    ListTest objectArray = new ListTest(arrayList);
    ListTest objectLinked = new ListTest(linkedList);

    System.out.format("\nВремя заполнения списка ArrayList 50000 элементами - %s\n",
        executionTimeArrayList);
    System.out.format("Время заполнения списка LinkedList 50000 элементами - %s\n",
        executionTimeLinkedList);

    // ArrayList
    System.out.printf(
        "\n\nВремя добавления элементов листа ArrayList: \nВремя добавления элемента в начале списка - %d\n"
            + "Время добавления элемента в середине списка - %d\nВремя добавления элемента в конце списка - %d\n",
        objectArray.getAddFirstElementTime(number), objectArray.getAddMiddleElementTime(number),
        objectArray.getAddLastElementTime(number));

    System.out.printf(
        "\n\nВремя изменения элементов листа ArrayList: \nВремя изменения элемента в начале списка - %d\n"
            + "Время изменения элемента в середине списка - %d\nВремя изменения элемента в конце списка - %d\n",
        objectArray.getSetFirstElementTime(number), objectArray.getSetMiddleElementTime(number),
        objectArray.getSetLastElementTime(number));

    System.out.printf(
        "\n\nВремя поиска элементов листа ArrayList: \nВремя поиска элемента в начале списка - %d\n"
            + "Время поиска элемента в середине списка - %d\nВремя поиска элемента в конце списка - %d\n",
        objectArray.getGetFirstElementTime(), objectArray.getGetMiddleElementTime(),
        objectArray.getGetLastElementTime());

    System.out.printf(
        "\n\nВремя удаления элементов листа ArrayList: \nВремя удаления элемента в начале списка - %d\n"
            + "Время удаления элемента в середине списка - %d\nВремя удаления элемента в конце списка - %d\n",
        objectArray.getRemoveFirstElementTime(), objectArray.getRemoveMiddleElementTime(),
        objectArray.getRemoveLastElementTime());

    // LinkedList
    System.out.printf(
        "\n\nВремя добавления элементов листа LinkedList: \nВремя добавления элемента в начале списка - %d\n"
            + "Время добавления элемента в середине списка - %d\nВремя добавления элемента в конце списка - %d\n",
        objectLinked.getAddFirstElementTime(number), objectLinked.getAddMiddleElementTime(number),
        objectLinked.getAddLastElementTime(number));

    System.out.printf(
        "\n\nВремя изменения элементов листа LinkedList: \nВремя изменения элемента в начале списка - %d\n"
            + "Время изменения элемента в середине списка - %d\nВремя изменения элемента в конце списка - %d\n",
        objectLinked.getSetFirstElementTime(number), objectLinked.getSetMiddleElementTime(number),
        objectLinked.getSetLastElementTime(number));

    System.out.printf(
        "\n\nВремя поиска элементов листа LinkedList: \nВремя поиска элемента в начале списка - %d\n"
            + "Время поиска элемента в середине списка - %d\nВремя поиска элемента в конце списка - %d\n",
        objectLinked.getGetFirstElementTime(), objectLinked.getGetMiddleElementTime(),
        objectLinked.getGetLastElementTime());

    System.out.printf(
        "\n\nВремя удаления элементов листа LinkedList: \nВремя удаления элемента в начале списка - %d\n"
            + "Время удаления элемента в середине списка - %d\nВремя удаления элемента в конце списка - %d\n",
        objectLinked.getRemoveFirstElementTime(), objectLinked.getRemoveMiddleElementTime(),
        objectLinked.getRemoveLastElementTime());
  }
}
