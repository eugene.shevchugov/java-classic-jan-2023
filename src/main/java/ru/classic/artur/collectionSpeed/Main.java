package ru.classic.artur.collectionSpeed;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

  private static Long timeToAddFirst(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.add(0, "dumb");
    return System.nanoTime() - startTime;
  }

  private static Long timeToAddMiddle(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.add(arrayExample.size() / 2, "meme");
    return System.nanoTime() - startTime;
  }

  private static Long timeToAddEnd(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.add("ending");
    return System.nanoTime() - startTime;
  }

  private static Long timeToPickFirst(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.get(0);
    return System.nanoTime() - startTime;
  }

  private static Long timeToPickMiddle(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.get(arrayExample.size() / 2);
    return System.nanoTime() - startTime;
  }

  private static Long timeToPickEnd(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.get(arrayExample.size() - 1);
    return System.nanoTime() - startTime;
  }

  private static Long timeToChangeFirst(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.set(0, "Changing");
    return System.nanoTime() - startTime;
  }

  private static Long timeToChangeMiddle(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.set(arrayExample.size() / 2, "Change");
    return System.nanoTime() - startTime;
  }

  private static Long timeToChangeEnd(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.set(arrayExample.size() - 1, "some");
    return System.nanoTime() - startTime;
  }

  private static Long timeToDeleteFirst(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.remove(0);
    return System.nanoTime() - startTime;
  }

  private static Long timeToDeleteMiddle(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.remove(arrayExample.size() / 2);
    return System.nanoTime() - startTime;
  }

  private static Long timeToDeleteEnd(List<String> arrayExample) {
    long startTime = System.nanoTime();
    arrayExample.remove(arrayExample.size() - 1);
    return System.nanoTime() - startTime;
  }

  public static void main(String[] args) {
    List<String> arrayExample = new ArrayList<>(31000);
    String listType = "ArrayList";
    long timeToAddStart = System.nanoTime();
    for (int i = 0; i < 30000; i++) {
      arrayExample.add(String.valueOf(i));
    }
    long timeToAdd = System.nanoTime() - timeToAddStart;
    System.out.printf(
        "Время заполнения %s %s наносекунд \nВремя добавления в начало %s %s наносекунд \nВремя добавления в середину %s %s наносекунд \nВремя добавления в конец %s %s наносекунд \nВремя получения элемента в начале %s %s наносекунд \nВремя получения элемента в середине %s %s наносекунд \nВремя получения элемента в конце %s %s наносекунд \nВремя изменения в начале %s %s наносекунд \nВремя изменения в середине %s %s наносекунд \nВремя изменения в конце %s %s наносекунд \nВремя удаления в начале %s %s наносекунд \nВремя удаления в середине %s %s наносекунд \nВремя удаления в конце %s %s наносекунд",
        listType,
        timeToAdd,
        listType,
        timeToAddFirst(arrayExample),
        listType,
        timeToAddMiddle(arrayExample),
        listType,
        timeToAddEnd(arrayExample),
        listType,
        timeToPickFirst(arrayExample),
        listType,
        timeToPickMiddle(arrayExample),
        listType,
        timeToPickEnd(arrayExample),
        listType,
        timeToChangeFirst(arrayExample),
        listType,
        timeToChangeMiddle(arrayExample),
        listType,
        timeToChangeEnd(arrayExample),
        listType,
        timeToDeleteFirst(arrayExample),
        listType,
        timeToDeleteMiddle(arrayExample),
        listType,
        timeToDeleteEnd(arrayExample));
  }
}
