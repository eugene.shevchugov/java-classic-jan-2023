package ru.classic.artur.methods;

public class Methods {

    public Methods(String sentence, int firstNumber, int secondNumber, int number) {
        stringPrint(sentence);
        System.out.println(difference(firstNumber, secondNumber));
        System.out.println(isEven(number));
    }

    public void stringPrint(String sentence) {
        System.out.println(sentence);
    }

    public int difference(int firstNumber, int secondNumber) {
        return Math.abs(firstNumber - secondNumber);
    }

    public boolean isEven(int number) {
        return number % 2 == 0;
    }

}
