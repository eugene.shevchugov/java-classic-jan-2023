package ru.classic.artur.hashSpeed;

import java.util.HashMap;
import java.util.Map;

public class Main {

  public static void main(String[] args) {

    Map<Human, String> humanMap = new HashMap<>();
    for (int i = 0; i < 10000; i++) {
      humanMap.put(new Woman(String.valueOf(i)), "12");
    }

    Woman testWoman = new Woman("Katya");
    Man testMan = new Man("Andrey");

    long womanCreateStart = System.nanoTime();
    humanMap.put(testWoman, "42");
    System.out.println(System.nanoTime() - womanCreateStart);

    long womanReadStart = System.nanoTime();
    humanMap.get(testWoman);
    System.out.println(System.nanoTime() - womanReadStart);

    long womanUpdateStart = System.nanoTime();
    humanMap.put(testWoman, "412");
    System.out.println(System.nanoTime() - womanUpdateStart);

    long womanDeleteStart = System.nanoTime();
    humanMap.remove(testWoman);
    System.out.println(System.nanoTime() - womanDeleteStart);

    humanMap.clear();

    for (int i = 0; i < 10000; i++) {
      humanMap.put(new Man(String.valueOf(i)), "15");
    }

    long manCreateStart = System.nanoTime();
    humanMap.put(testMan, "102");
    System.out.println(System.nanoTime() - manCreateStart);

    long manReadStart = System.nanoTime();
    humanMap.get(testMan);
    System.out.println(System.nanoTime() - manReadStart);

    long manUpdateStart = System.nanoTime();
    humanMap.put(testMan, "1002");
    System.out.println(System.nanoTime() - manUpdateStart);

    long manDeleteStart = System.nanoTime();
    humanMap.remove(testMan);
    System.out.println(System.nanoTime() - manDeleteStart);

  }

}
