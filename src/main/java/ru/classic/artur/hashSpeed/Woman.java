package ru.classic.artur.hashSpeed;


public class Woman implements Human{

  private String name;

  @Override
  public String toString() {
    return "Woman{" +
        "name='" + name + '\'' +
        '}';
  }

  public Woman (String name){
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Woman woman = (Woman) o;
    return name.equals(woman.name);
  }

  @Override
  public int hashCode() {
    return 1;
  }
}
