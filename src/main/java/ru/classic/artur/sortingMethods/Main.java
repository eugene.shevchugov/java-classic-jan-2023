package ru.classic.artur.sortingMethods;

import java.util.Arrays;

public class Main {

  public static int[] bubbleSort(int[] array) {
    int buffer;
    boolean sort = false;
    while (!sort) {
      sort = true;
      for (int i = 0; i < array.length - 1; i++) {
        if (array[i] > array[i + 1]) {
          sort = false;
          buffer = array[i];
          array[i] = array[i + 1];
          array[i + 1] = buffer;
        }
      }
    }
    return array;
  }

  public static int[] quickSort(int[] array, int start, int end) {
    if (array.length == 0 || start >= end) {
      return array;
    }

    int support = array[start + (end - start) / 2];

    int i = start, j = end;
    while (i <= j) {
      while (array[i] < support) {
        i++;
      }
      while (array[j] > support) {
        j--;
      }
      if (i <= j) {
        int buffer = array[i];
        array[i] = array[j];
        array[j] = buffer;
        i++;
        j--;
      }
    }
    if (start < j) {
      quickSort(array, start, j);
    }
    if (end > i) {
      quickSort(array, i, end);
    }
    return array;
  }

  public static String foo(String[] stringArray) {
    int i = 0;
    StringBuilder sb = new StringBuilder();
    while (stringArray.length > 0 && i < stringArray[0].length()) {
      char buffer = stringArray[0].charAt(i);
      for (int j = 1; j < stringArray.length; j++) {
        if (i < stringArray[j].length() && stringArray[j].charAt(i) != buffer) {
          return sb.toString();
        }
      }
      i++;
      sb.append(buffer);
    }
    return sb.toString();
  }

  public static int[] radSort(int[] numbers){
    int maxDigits = maxDigits(numbers);
    int divider = 1;
    for(int i = 0; i< maxDigits; i++){
      numbers = sorting(numbers,divider);
      divider*= 10;
    }
    return numbers;
  }

  private static int maxDigits(int [] numbers){
    int result = 1;
    for(int number : numbers) {
      int digits = numberOfDigits(number);
      if (digits > result) {
        result = digits;
      }
    }
    return result;
  }

  private static int numberOfDigits(int number) {
    return (int) Math.log10(number) + 1;
  }

  private static int[] sorting(int[] numbers, int divider){
    int[] minMaxKey = minMaxKey(numbers, divider);
    int minKey = minMaxKey[0];
    int maxKey = minMaxKey[1];
    int n = maxKey - minKey + 1;
    int [] supp = new int[n];
    for (int number : numbers){
      supp[getDigit(number,divider) - minKey] += 1;
    }
    int size = numbers.length;
    for(int i = supp.length -1; i>= 0; i--){
      size -= supp[i];
      supp[i] = size;
    }
    int[] result = new int[numbers.length];
    for (int number: numbers){
      result[supp[getDigit(number, divider) - minKey]] = number;
      supp[getDigit(number, divider) - minKey] += 1;
    }
    return result;
  }

  private static int[] minMaxKey(int [] numbers, int divider){
    int minKey = getDigit(numbers[0], divider);
    int maxKey = minKey;
    for(int number : numbers) {
      int digit = getDigit(number, divider);
      if(digit < minKey){
        minKey = digit;
      }
      if(digit > maxKey){
        maxKey = digit;
      }
    }
    return new int[] {minKey, maxKey};
  }

  private static int getDigit(int number, int divider){
    return number % (divider * 10) / divider;
  }

  public static void main(String[] args) {
    int[] array1 = {412, 12, 1, 4, 15, 2, 3, 6, 1002, 1001};
    System.out.println(Arrays.toString(bubbleSort(array1)));
    int[] array2 = {412, 12, 1, 4, 15, 2, 3, 6, 1002, 1001};
    System.out.println(Arrays.toString(quickSort(array2, 0, array2.length - 1)));
    String[] stringArray = {"sort", "sore", "sornyak"};
    System.out.println(foo(stringArray));
    int[] array3 = {412, 12, 1, 4, 15, 2, 3, 6, 1002, 1001};
    System.out.println(Arrays.toString(radSort(array3)));
  }
}
