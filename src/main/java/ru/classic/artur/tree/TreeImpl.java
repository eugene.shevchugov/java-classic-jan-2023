package ru.classic.artur.tree;

import java.util.ArrayList;
import java.util.Collection;

public class TreeImpl<E extends Comparable<E>> implements Tree<E> {
  private TreeImpl<E> left;
  private TreeImpl<E> right;

  private E value;

  public TreeImpl(E value) {
    this.value = value;
  }

  public StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
    if (right != null) {
      right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false,
          sb);
    }
    sb.append(prefix).append(isTail ? "└── " : "┌── ").append(value.toString()).append("\n");
    if (left != null) {
      left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
    }
    return sb;
  }

  @Override
  public String toString() {
    return this.toString(new StringBuilder(), true, new StringBuilder()).toString();
  }

  @Override
  public void add(E elem) {
    if (value == null) {
      value = elem;
    } else {
      if (value.compareTo(elem) > 0) {
        if (left == null) {
          left = new TreeImpl<>(elem);
        } else {
          left.add(elem);
        }
      } else if (value.compareTo(elem) < 0) {
        if (right == null) {
          right = new TreeImpl<>(elem);
        } else {
          right.add(elem);
        }
      }
    }
  }


  @Override
  public void delete(E elem) {
    if (right != null && right.value.equals(elem)) {
      deleteElem("right");
    } else if (left != null && left.value.equals(elem)) {
      deleteElem("left");
    } else if (left != null && value.compareTo(elem) > 0) {
      left.delete(elem);
    } else if (right != null && value.compareTo(elem) < 0) {
      right.delete(elem);
    }
  }

  @Override
  public Collection<E> getAll() {
    Collection<E> result = new ArrayList<>();
    collectionAdd(result);
    return result;
  }

  @Override
  public boolean contains(E elem) {
    if (value.equals(elem)) {
      return true;
    }

    if (value.compareTo(elem) > 0 && left != null) {
      return left.contains(elem);
    }

    if (value.compareTo(elem) < 0 && right != null) {
      return right.contains(elem);
    }

    return false;
  }

  @Override
  public void clear() {
    left = null;
    right = null;
    value = null;
  }

  private void deleteElem(String direction) {
    switch (direction) {
      case "left" -> {
        TreeImpl<E> elemToDel = left;
        left = null;
        if (elemToDel.right != null) {
          addAll(elemToDel.right);
        }

        if (elemToDel.left != null) {
          addAll(elemToDel.left);
        }
      }

      case "right" -> {
        TreeImpl<E> elemToDel = right;
        right = null;

        if (elemToDel.right != null) {
          addAll(elemToDel.right);
        }

        if (elemToDel.left != null) {
          addAll(elemToDel.left);
        }
      }
    }
  }

  private void addAll(TreeImpl<E> elems) {
    add(elems.value);

    if (elems.left != null) {
      addAll(elems.left);
    }

    if (elems.right != null) {
      addAll(elems.right);
    }
  }

  private void collectionAdd(Collection<E> result){
    if (left != null) {
      left.collectionAdd(result);
    }

    if (right != null) {
      right.collectionAdd(result);
    }

    result.add(value);
  }

}
