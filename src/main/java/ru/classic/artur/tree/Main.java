package ru.classic.artur.tree;

public class Main {

  public static void main(String[] args) {
    TreeImpl<Integer> tree = new TreeImpl<>(10);
    tree.add(10);
    tree.add(9);
    tree.add(11);
    tree.add(7);
    tree.add(8);
    tree.add(3);
    tree.add(4);
    tree.add(5);
    tree.add(6);
    tree.add(21);
    tree.add(61);
    tree.add(12);
    tree.add(17);
    System.out.println(tree);
    System.out.println(tree.contains(9));
    System.out.println(tree.getAll());
    System.out.println("--------------------------------------------------------------------");
    tree.delete(12);
    System.out.println(tree);
    tree.clear();
  }

}
