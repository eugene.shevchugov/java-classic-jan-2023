package ru.classic.artur.tree;

import java.util.Collection;

public interface Tree<E extends Comparable<E>> {

  void add(E elem);

  void delete(E elem);

  Collection<E> getAll();

  boolean contains(E elem);

  void clear();
}
