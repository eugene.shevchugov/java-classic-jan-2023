package ru.classic.string;

public interface StringSimplifier {

  /**
   * @param summary
   * @return String as "Summary is ${summary} rub."
   */
  String summaryForFrontView(Integer summary);

  /**
   * @param date as "*[\D+]dd*[\D+]mm*[\D+]yyyy"
   * @return String as "dd.mm.yyyy"
   */
  String formatDate(String date);

  // expected to clean any symbols exclude eng alphabet and spaces between words

  /**
   * @param text as any symbols
   * @return String contains only english symbols and spaces between words
   */
  String cleanTrash(String text);

  /**
   * @param array
   * @return String as "${elem}\tsum = ${elem + sumOfPreviousElems}\n etc."
   */
  String print(Integer... array);

  /**
   *
   * @param array
   * @return String as overall beginning of all string in array
   * @examples
   *  array = {"poor", "polite", "police"} returns "po"
   *  array = {"asd", "asd", "pokv"} returns ""
   */
  String getOverallBeginning(String[] array);
}
